import boto3
from datetime import datetime

# 아시아 태평양(서울) region
region = 'ap-northeast-2'

week = ["월", "화", "수", "목", "금", "토", "일"]


def lambda_handler(event, context):
    # 람다 호출된시점의 시간을 구합니다.
    print(" ==================== EC2 Auto On/Off Start ====================")
    now_date = week[datetime.today().weekday()]
    now_hour = int(datetime.now().strftime('%H'))

    print("현재 " + now_date + "요일 " + str(now_hour) + "시 입니다.")

    # EC2 인스턴스 태그 조회
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.describe_tags(
        Filters=[
            {
                'Name': 'resource-type',
                'Values': ['instance']
            }
        ]
    )

    # 값 임시 저장
    enable_instances = []

    # AUTO_STOP_ENABLE 태그가 true인 값만 추출
    for tag in response['Tags']:
        if tag['Key'] == "AUTOSTOP_ENABLE" and tag['Value'].lower() == "true":
            enable_instances.append(tag['ResourceId'])

    print("enable_instances : " + str(enable_instances))

    for instance in enable_instances:
        try:
            # 중지 인스턴스 호출(18시 이후)
            if now_hour < 7 or now_hour >= 18:
                ec2.stop_instances(InstanceIds=[instance])
                print("Instance " + instance + " is Stop.")
            elif now_hour >= 7 and now_hour < 18:
                # 시작 인스턴스 호출(7시 이후)
                ec2.start_instances(InstanceIds=[instance])
                print("Instance " + instance + " is Start.")
        except Exception as ex:
            print(ex)

    print(" ==================== EC2 Auto On/Off Start ====================")